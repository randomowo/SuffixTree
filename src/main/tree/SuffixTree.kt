package main.tree

import main.impl.Node
import main.impl.SuffixTreeInterface
import java.io.File
import java.io.IOException

class SuffixTree : SuffixTreeInterface {
    override val nodes = mutableListOf<Node>(Node())
    private var leafs = 0

    fun addFile(file: String) {
        try {
            File(file).readLines().forEach { addSentence(it) }
        } catch (e: IOException) {
            println("file not found")
        }
    }

    fun addSentence(sentence: String) {
        sentence.split(" ").forEach { addWord(it) }
    }

    override fun addWord(word: String) {
        for (length in 0 until word.length) {
            addSuffix(word.substring(length))
        }
    }

    private fun addSuffix(suffix: String) {
        var exist = false
        for (childIndex in nodes[0].ch) {
            if (nodes[childIndex].sub.first() == suffix.first()) {
                exist = true // do not add new node
                addSplit(childIndex, suffix)
            }
        }
        if (!exist) {
            nodes.add(Node().apply { sub = suffix })
            nodes[0].ch.add(++leafs)
        }
    }

    private fun addSplit(nodeIndex: Int, suffix: String) {
        val nodeSub = nodes[nodeIndex].sub
        val match = subMatchInt(nodeSub, suffix)
        if (match > 0) {
            if (!(match == nodeSub.length && search(nodeIndex, suffix.substring(match)))) {
                val children = nodes[nodeIndex].ch
                nodes[nodeIndex].sub = nodeSub.substring(0, match)
                if (nodeSub.length != match) {
                    nodes.add(Node().apply { sub = nodeSub.substring(match) })
                    leafs++
                    if (!nodes[nodeIndex].isLeaf()) {
                        nodes[nodeIndex].ch = mutableListOf()
                        nodes[leafs].ch.addAll(children)
                    }
                    nodes[nodeIndex].ch.add(leafs)
                }
                if (suffix.length != match) {
                    nodes.add(Node().apply { sub = suffix.substring(match) })
                    nodes[nodeIndex].ch.add(++leafs)
                }
            } else {
                for (childIndex in nodes[nodeIndex].ch) addSplit(childIndex, suffix.substring(match))
            }
        }
    }

    override fun longestRepeatedSub(): String {
        fun lrs(nodeIndex: Int): String {
            var longest = ""
            for (childIndex in nodes[nodeIndex].ch) {
                var long = ""
                if (nodes[childIndex].isLeaf()) continue
                val child = nodes[childIndex].sub
                if (child.length > long.length) long += (child + lrs(childIndex))
                if (long.length > longest.length) longest = long
            }
            return longest
        }
        return lrs(0)
    }

    override fun search(searchWord: String): Boolean {
        return search(0, searchWord)
    }

    private fun search(child: Int, searchSub: String): Boolean {
        if (searchSub.isEmpty()) return true
        for (chIndex in nodes[child].ch) { // review all node's children
            val sub = nodes[chIndex].sub
            val match = subMatchInt(sub, searchSub)
            if (match > 0) {
                if (nodes[chIndex].isLeaf() && searchSub.length == match) return true
                if (search(chIndex, searchSub.substring(match))) return true
            }
        }
        return false
    }


    private fun subMatchInt(sub: String, searchSub: String): Int { // fun for matching two strings
        val shorter = if (sub.length < searchSub.length) sub.length else searchSub.length
        for (index in 0 until shorter)
            if (sub[index] != searchSub[index]) return index
        return shorter
    }

    override fun visualise() {
        if (this.isEmpty()) {
            println("<empty>")
            return
        }

        fun f(n: Int, pre: String) {
            val children = nodes[n]
            if (children.isLeaf()) {
                println("╴ ${children.sub}")
                return
            }
            println("┐ ${nodes[n].sub}")
            for (c in children.ch.dropLast(1)) {
                print("$pre├─")
                f(c, "$pre│ ")
            }
            print("$pre└─")
            f(children.ch.last(), "$pre  ")
        }

        f(0, "")
    }

    private fun isEmpty(): Boolean = nodes[0].isLeaf()

    fun empty() {
        nodes.removeAll(this.nodes)
        leafs=0
        nodes.add(Node())
    }
}
