package main.impl

interface SuffixTreeInterface {
    val nodes: MutableList<Node>
    fun addWord(word: String)
    fun visualise()
    fun search(searchWord: String): Boolean
    fun longestRepeatedSub(): String
}

class Node {
    var sub = ""
    var ch = mutableListOf<Int>()
    fun isLeaf(): Boolean = this.ch.isEmpty()
}
