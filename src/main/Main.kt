package main

import main.tree.SuffixTree

fun main(args: Array<String>) {
    val tree = SuffixTree()
    println(
        """
   _____        __  __ _   _______                        _____ _      _____
  / ____|      / _|/ _(_) |__   __|                      / ____| |    |_   _|
 | (___  _   _| |_| |_ ___  _| |_ __ ___  ___   ______  | |    | |      | |
  \___ \| | | |  _|  _| \ \/ / | '__/ _ \/ _ \ |______| | |    | |      | |
  ____) | |_| | | | | | |>  <| | | |  __/  __/          | |____| |____ _| |_
 |_____/ \__,_|_| |_| |_/_/\_\_|_|  \___|\___|           \_____|______|_____|
                                                            made by randomowo
        """.trimIndent()
    )
    var input = ""
    while (input.toLowerCase() != "exit") {
        print("> ")
        input = readLine() ?: ""
        choice(input.split(" ").toTypedArray(), tree)
    }

}

fun choice(input: Array<String>, tree: SuffixTree) =
    when (input[0]) {
        "exit" -> println("bye")
        "-lr" -> println(tree.longestRepeatedSub())
        "-n", "--new" -> tree.empty()
        "-v", "--visualise" -> tree.visualise()
        "-h", "--help" -> println(
            """
            [-w [string]] [-s [sentence]] [-S [search word]] [-lr] [-n] [-v] [-h] [-f [file name]]
            [--addsentence [sentence]] [--addWord [string]] [--search [search word]]
            [--addfile [file name]] [--longestrepeat] [--new] [--visualise] [--help] [exit]
        """.trimIndent()
        )
        "-f", "--addfile" -> ifErr(input, 2) { tree.addFile(input[1]) }
        "-w", "--addword" -> ifErr(input, 2) { tree.addWord(input[1]) }
        "-s", "--addsentence" -> ifErr(input, 2) {
            val str = StringBuilder()
            for (index in 1 until input.size)
                str.append("${input[index]} ")
            tree.addSentence(str.toString())
        }
        "-S", "--search" -> ifErr(input, 2) {
            if (tree.search(input[1])) println("+") else
                println("-")
        }
        else -> ifErr()
    }

fun <T> ifErr(input: Array<String> = arrayOf(), size: Int = 0, func: () -> T) {
    if (input.size >= size) func.invoke() else println("err")
}

fun ifErr() = println("err")