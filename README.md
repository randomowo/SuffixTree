# Suffix Tree cli
#### dependencies:
* __[ApacheMaven](https://maven.apache.org/install.html) >= 3.6.0__ 
* __[java](https://www.oracle.com/technetwork/java/javase/downloads/index.html) >= 11.0.1__
* __kotlin >= 1.3.10__

I think you can use older packages. Give a try. 
### Installation:
     git clone --recursive https://github.com/randomowo/SuffixTree.git
     
     cd SuffixTree
     
     mvn install clean package
     
     java -jar target/SuffixTree-${version}-jar-with-dependencies.jar


## You can:
- [x] add a word via ```
                     -w [string] or --addword [string
                     ```

- [x] add a sentence via ```
                         -s [sentence] or --addsentence [sentence]
                         ```

- [x] add a file via ```
                     -f [fine name] or --addfile [file name] 
                     ``` 
                     
    __for large file -> ```java.lang.OutOfMemoryError: Java heap space```__
- [x] search a string in a SuffixTree via ```
                                          -S [string] or --search [string]
                                          ```

- [X] find a longest repeat substring via ```
                                         -lrs
                                          ```

- [ ] find a longest common substring of two string via ```
                                               -lcs [[string1] [string2]]
                                                        ```

- [x] visualise a Suffix tree via ```
                                -v or -visualise
                                ```
- [x] see all commands ```
                       -h or --help
                       ```

- [x] and of course leave via ```
                              exit
                              ```
                              
 ## TODO:
 - [ ] make own String class for make adding large file works
 - [ ] Ukkonen algorithm
