package main

import main.tree.SuffixTree
import org.junit.Assert.assertEquals
import org.junit.Test

class LongestRepeatedSubTest {
    @Test
    fun lrsTest1() {
        val tree = SuffixTree()
        val lrs = "joke"
        tree.addWord("jokejoke")
        assertEquals(tree.longestRepeatedSub(), lrs)
    }

    @Test
    fun lrsTest2() {
        val tree = SuffixTree()
        val lrs = "issi"
        tree.addWord("mississipi")
        assertEquals(tree.longestRepeatedSub(), lrs)
    }

    @Test
    fun lrsTest3() {
        val tree = SuffixTree()
        val lrs = "GA"
        tree.addWord("GATAGACA")
        assertEquals(tree.longestRepeatedSub(), lrs)
    }

    @Test
    fun lrsTest4() {
        val tree = SuffixTree()
        val lrs = "ANA"
        tree.addWord("BANANA")
        assertEquals(tree.longestRepeatedSub(), lrs)
    }

    @Test
    fun lrsTest5() {
        val tree = SuffixTree()
        val lrs = "jokejoke"
        tree.addWord("jokejokejoke")
        assertEquals(tree.longestRepeatedSub(), lrs)
    }
}