package main

import main.tree.SuffixTree
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.File
import java.io.IOException

class AddTest {

    @Test
    fun addWord1() {
        val tree = SuffixTree()
        val word = "question"
        tree.addWord(word)
        addingWordTest(word, tree)
    }

    @Test
    fun addWord2() {
        val tree = SuffixTree()
        val word = "ssssssssss"
        tree.addWord(word)
        addingWordTest(word, tree)
    }

    @Test
    fun addWord3() {
        val tree = SuffixTree()
        val word = "Lancelot"
        tree.addWord(word)
        addingWordTest(word, tree)
    }

    @Test
    fun addWord4() {
        val tree = SuffixTree()
        val word = "Tall"
        tree.addWord(word)
        addingWordTest(word, tree)
    }

    @Test
    fun addWord5() {
        val tree = SuffixTree()
        val word = "CAPSLOCK"
        tree.addWord(word)
        addingWordTest(word, tree)
    }

    @Test
    fun addSentence1() {
        val tree = SuffixTree()
        val sentence = "Sir Lancelot the greatest"
        tree.addSentence(sentence)
        addingSentenceTest(sentence, tree)
    }

    @Test
    fun addSentence2() {
        val tree = SuffixTree()
        val sentence = "42: THE ANSWER TO LIFE, THE UNIVERSE AND EVERYTHING"
        tree.addSentence(sentence)
        addingSentenceTest(sentence, tree)
    }

    @Test
    fun addSentence3() {
        val tree = SuffixTree()
        // Reptile-like quote (Skyrim npc - Madesi)
        val sentence = "Pleassse, milady, it was only becaussse he threatened me." +
                " He ssaid he'd have me thrown in the prisonssss if I didn't give him what he wanted!" +
                " I can't afford to just give my jewelry away!"
        tree.addSentence(sentence)
        addingSentenceTest(sentence, tree)
    }

    @Test
    fun addFromFile1() {
        val tree = SuffixTree()
        val fileName = "temp.txt"
        val text =
            """SOCRATES: I dare say that you may be surprised to find, O son of Cleinias, that I, who am your first lover, not having spoken to you for many years, when the rest of the world were wearying you with their attentions, am the last of your lovers who still speaks to you. The cause of my silence has been that I was hindered by a power more than human, of which I will some day explain to you the nature; this impediment has now been removed; I therefore here present myself before you, and I greatly hope that no similar hindrance will again occur. Meanwhile, I have observed that your pride has been too much for the pride of your admirers; they were numerous and high-spirited, but they have all run away, overpowered by your superior force of character; not one of them remains. And I want you to understand the reason why you have been too much for them. You think that you have no need of them or of any other man, for you have great possessions and lack nothing, beginning with the body, and ending with the soul. In the first place, you say to yourself that you are the fairest and tallest of the citizens, and this every one who has eyes may see to be true; in the second place, that you are among the noblest of them, highly connected both on the father’s and the mother’s side, and sprung from one of the most distinguished families in your own state, which is the greatest in Hellas, and having many friends and kinsmen of the best sort, who can assist you when in need; and there is one potent relative, who is more to you than all the rest, Pericles the son of Xanthippus, whom your father left guardian of you, and of your brother, and who can do as he pleases not only in this city, but in all Hellas, and among many and mighty barbarous nations. Moreover, you are rich; but I must say that you value yourself least of all upon your possessions. And all these things have lifted you up; you have overcome your lovers, and they have acknowledged that you were too much for them. Have you not remarked their absence? And now I know that you wonder why I, unlike the rest of them, have not gone away, and what can be my motive in remaining.
ALCIBIADES: Perhaps, Socrates, you are not aware that I was just going to ask you the very same question-What do you want? And what is your motive in annoying me, and always, wherever I am, making a point of coming? (Compare Symp.) I do really wonder what you mean, and should greatly like to know.
SOCRATES: Then if, as you say, you desire to know, I suppose that you will be willing to hear, and I may consider myself to be speaking to an auditor who will remain, and will not run away? ALCIBIADES: Certainly, let me hear. SOCRATES: You had better be careful, for I may very likely be as unwilling to end as I have hitherto been to begin. ALCIBIADES: Proceed, my good man, and I will listen. SOCRATES: I will proceed; and, although no lover likes to speak with one who has no feeling of love in him (compare Symp.), I will make an effort, and tell you what I meant: My love, Alcibiades, which I hardly like to confess, would long ago have passed away, as I flatter myself, if I saw you loving your good things, or thinking that you ought to pass life in the enjoyment of them. But I shall reveal other thoughts of yours, which you keep to yourself; whereby you will know that I have always had my eye on you. Suppose that at this moment some God came to you and said: Alcibiades, will you live as you are, or die in an instant if you are forbidden to make any further acquisition?–I verily believe that you would choose death. And I will tell you the hope in which you are at present living: Before many days have elapsed, you think that you will come before the Athenian assembly, and will prove to them that you are more worthy of honour than Pericles, or any other man that ever lived, and having proved this, you will have the greatest power in the state. When you have gained the greatest power among us, you will go on to other Hellenic states, and not only to Hellenes, but to all the barbarians who inhabit the same continent with us. And if the God were then to say to you again: Here in Europe is to be your seat of empire, and you must not cross over into Asia or meddle with Asiatic affairs, I do not believe that you would choose to live upon these terms; but the world, as I may say, must be filled with your power and name-no man less than Cyrus and Xerxes is of any account with you. Such I know to be your hopes-I am not guessing only-and very likely you, who know that I am speaking the truth, will reply, Well, Socrates, but what have my hopes to do with the explanation which you promised of your unwillingness to leave me? And that is what I am now going to tell you, sweet son of Cleinias and Dinomache. The explanation is, that all these designs of yours cannot be accomplished by you without my help; so great is the power which I believe myself to have over you and your concerns; and this I conceive to be the reason why the God has hitherto forbidden me to converse with you, and I have been long expecting his permission. For, as you hope to prove your own great value to the state, and having proved it, to attain at once to absolute power, so do I indulge a hope that I shall be the supreme power over you, if I am able to prove my own great value to you, and to show you that neither guardian, nor kinsman, nor any one is able to deliver into your hands the power which you desire, but I only, God being my helper. When you were young (compare Symp.) and your hopes were not yet matured, I should have wasted my time, and therefore, as I conceive, the God forbade me to converse with you; but now, having his permission, I will speak, for now you will listen to me.
ALCIBIADES: Your silence, Socrates, was always a surprise to me. I never could understand why you followed me about, and now that you have begun to speak again, I am still more amazed. Whether I think all this or not, is a matter about which you seem to have already made up your mind, and therefore my denial will have no effect upon you. But granting, if I must, that you have perfectly divined my purposes, why is your assistance necessary to the attainment of them? Can you tell me why?
SOCRATES: You want to know whether I can make a long speech, such as you are in the habit of hearing; but that is not my way. I think, however, that I can prove to you the truth of what I am saying, if you will grant me one little favour.
ALCIBIADES: Yes, if the favour which you mean be not a troublesome one.
SOCRATES: Will you be troubled at having questions to answer?
ALCIBIADES: Not at all.
SOCRATES: Then please to answer.
ALCIBIADES: Ask me.
SOCRATES: Have you not the intention which I attribute to you?
ALCIBIADES: I will grant anything you like, in the hope of hearing what more you have to say.
SOCRATES: You do, then, mean, as I was saying, to come forward in a little while in the character of an adviser of the Athenians? And suppose that when you are ascending the bema, I pull you by the sleeve and say, Alcibiades, you are getting up to advise the Athenians-do you know the matter about which they are going to deliberate, better than they?–How would you answer?
ALCIBIADES: I should reply, that I was going to advise them about a matter which I do know better than they.
SOCRATES: Then you are a good adviser about the things which you know?
ALCIBIADES: Certainly.
SOCRATES: And do you know anything but what you have learned of oth-
ers, or found out yourself?
ALCIBIADES: That is all.
SOCRATES: And would you have ever learned or discovered anything, if
you had not been willing either to learn of others or to examine yourself?
ALCIBIADES: I should not.
SOCRATES: And would you have been willing to learn or to examine what
you supposed that you knew?
ALCIBIADES: Certainly not.
SOCRATES: Then there was a time when you thought that you did not
know what you are now supposed to know?
ALCIBIADES: Certainly.
SOCRATES: I think that I know tolerably well the extent of your acquirements; and you must tell me if I forget any of them: according to my recollection, you learned the arts of writing, of playing on the lyre, and of wrestling; the flute you never would learn; this is the sum of your accomplishments, unless there were some which you acquired in secret; and I think that secrecy was hardly possible, as you could not have come out of your door, either by day or night, without my seeing you.
ALCIBIADES: Yes, that was the whole of my schooling.
SOCRATES: And are you going to get up in the Athenian assembly, and give them advice about writing?
ALCIBIADES: No, indeed.
SOCRATES: Or about the touch of the lyre?
ALCIBIADES: Certainly not.
SOCRATES: And they are not in the habit of deliberating about wrestling,
in the assembly?
ALCIBIADES: Hardly.
SOCRATES: Then what are the deliberations in which you propose to advise
them? Surely not about building?
ALCIBIADES: No.
SOCRATES: For the builder will advise better than you will about that?
ALCIBIADES: He will.
SOCRATES: Nor about divination?
ALCIBIADES: No.
SOCRATES: About that again the diviner will advise better than you will?
ALCIBIADES: True.
SOCRATES: Whether he be little or great, good or ill-looking, noble or
ignoble-makes no difference. ALCIBIADES: Certainly not.
SOCRATES: A man is a good adviser about anything, not because he has riches, but because he has knowledge?
ALCIBIADES: Assuredly.
SOCRATES: Whether their counsellor is rich or poor, is not a matter which will make any difference to the Athenians when they are deliberating about the health of the citizens; they only require that he should be a physician.
ALCIBIADES: Of course.
SOCRATES: Then what will be the subject of deliberation about which you will be justified in getting up and advising them?
ALCIBIADES: About their own concerns, Socrates.
SOCRATES: You mean about shipbuilding, for example, when the question is what sort of ships they ought to build?
ALCIBIADES: No, I should not advise them about that.
SOCRATES: I suppose, because you do not understand shipbuilding:–is that the reason?
ALCIBIADES: It is.
SOCRATES: Then about what concerns of theirs will you advise them?
ALCIBIADES: About war, Socrates, or about peace, or about any other
concerns of the state.
SOCRATES: You mean, when they deliberate with whom they ought to
make peace, and with whom they ought to go to war, and in what manner?
ALCIBIADES: Yes.
SOCRATES: And they ought to go to war with those against whom it is
better to go to war?
ALCIBIADES: Yes.
SOCRATES: And when it is better?
ALCIBIADES: Certainly.
SOCRATES: And for as long a time as is better?
ALCIBIADES: Yes.
SOCRATES: But suppose the Athenians to deliberate with whom they ought
to close in wrestling, and whom they should grasp by the hand, would you, or the master of gymnastics, be a better adviser of them?
ALCIBIADES: Clearly, the master of gymnastics.
SOCRATES: And can you tell me on what grounds the master of gymnastics would decide, with whom they ought or ought not to close, and when and how? To take an instance: Would he not say that they should wrestle with those against whom it is best to wrestle?
ALCIBIADES: Yes.
SOCRATES: And as much as is best?
ALCIBIADES: Certainly.
SOCRATES: And at such times as are best?
ALCIBIADES: Yes.
SOCRATES: Again; you sometimes accompany the lyre with the song and
dance?
ALCIBIADES: Yes.
SOCRATES: When it is well to do so?
ALCIBIADES: Yes.
SOCRATES: And as much as is well?
ALCIBIADES: Just so.
SOCRATES: And as you speak of an excellence or art of the best in wrestling, and of an excellence in playing the lyre, I wish you would tell me what this latter is;–the excellence of wrestling I call gymnastic, and I want to know what you call the other.
ALCIBIADES: I do not understand you.
SOCRATES: Then try to do as I do; for the answer which I gave is universally right, and when I say right, I mean according to rule.
ALCIBIADES: Yes.
SOCRATES: And was not the art of which I spoke gymnastic?
ALCIBIADES: Certainly.
SOCRATES: And I called the excellence in wrestling gymnastic?
ALCIBIADES: You did.
SOCRATES: And I was right?
ALCIBIADES: I think that you were.
SOCRATES: Well, now,–for you should learn to argue prettily-let me ask
you in return to tell me, first, what is that art of which playing and singing, and stepping properly in the dance, are parts,–what is the name of the whole? I think that by this time you must be able to tell.
ALCIBIADES: Indeed I cannot.
SOCRATES: Then let me put the matter in another way: what do you call the Goddesses who are the patronesses of art?
ALCIBIADES: The Muses do you mean, Socrates?
SOCRATES: Yes, I do; and what is the name of the art which is called after them?
ALCIBIADES: I suppose that you mean music.
SOCRATES: Yes, that is my meaning; and what is the excellence of the art of music, as I told you truly that the excellence of wrestling was gymnastic-what is the excellence of music-to be what?
ALCIBIADES: To be musical, I suppose.
SOCRATES: Very good; and now please to tell me what is the excellence of war and peace; as the more musical was the more excellent, or the more gymnastical was the more excellent, tell me, what name do you give to the more excellent in war and peace?
ALCIBIADES: But I really cannot tell you.
SOCRATES: But if you were offering advice to another and said to him- This food is better than that, at this time and in this quantity, and he said to you-What do you mean, Alcibiades, by the word ‘better’? you would have no difficulty in replying that you meant ‘more wholesome,’ although you do not profess to be a physician: and when the subject is one of which you profess to have knowledge, and about which you are ready to get up and advise as if you knew, are you not ashamed, when you are asked, not to be able to answer the question? Is it not disgraceful?
ALCIBIADES: Very.
SOCRATES: Well, then, consider and try to explain what is the meaning of ‘better,’ in the matter of making peace and going to war with those against whom you ought to go to war? To what does the word refer?
ALCIBIADES: I am thinking, and I cannot tell.
SOCRATES: But you surely know what are the charges which we bring against one another, when we arrive at the point of making war, and what name we give them?"""
        File(fileName).createNewFile()
        File(fileName).writeText(text)
        tree.addFile(fileName)
        addingFromFileTest(fileName, tree)
        File(fileName).delete()
    }

    //@Test
    //java.lang.OutOfMemoryError: Java heap space
    fun addFromFile3() {
        val tree = SuffixTree()
        val fileName = "resources/fileTest/lolita.txt"
        tree.addFile(fileName)
        addingFromFileTest(fileName, tree)
    }
}

fun addingWordTest(word: String, tree: SuffixTree) {
    for (index in 0 until word.length) {
        assertTrue(tree.search(word.substring(index)))
    }
}

fun addingSentenceTest(sentence: String, tree: SuffixTree) {
    for (word in sentence.split(" ")) addingWordTest(word, tree)
}

fun addingFromFileTest(fileName: String, tree: SuffixTree) {
    try {
        File(fileName).readLines().forEach { addingSentenceTest(it, tree) }
    } catch (e: IOException) {
        throw e
    }
}