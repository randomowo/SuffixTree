package main

import main.tree.SuffixTree
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class SearchTest {

    @Test
    fun searchInWord() {
        val tree = SuffixTree()
        tree.addWord("search")
        assertTrue(tree.search("sear"))
        assertTrue(tree.search("rch"))
        assertTrue(tree.search("ear"))
        assertFalse(tree.search("m"))
        assertFalse(tree.search("question"))
        assertFalse(tree.search("42"))
    }

    @Test
    fun searchInSentence() {
        val tree = SuffixTree()
        tree.addSentence("42: THE ANSWER TO LIFE, THE UNIVERSE AND EVERYTHING")
        assertTrue(tree.search("42"))
        assertTrue(tree.search("UNI"))
        assertTrue(tree.search("EVERY"))
        assertTrue(tree.search(","))
        assertFalse(tree.search("NOT"))
    }

    //@Test
    //java.lang.OutOfMemoryError: Java heap space
    fun searchInFile() {
        val tree = SuffixTree()
        tree.addFile("resources/fileTest/lolita.txt")
        tree.search("realize")
    }
}